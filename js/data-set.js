        String.prototype.format = function() {
            var formatted = this;
            for(arg in arguments) {
                formatted = formatted.replace("{" + arg + "}", arguments[arg]);
            }
            return formatted;
        };

        function InputValid (text){
            var re =/^[1-9]+\d*$/;
            return re.test(text);
        }

        function GetTableHTML(index_record,fibbo_record, fact_record, area_record, vol_record, sum_record) {
            var TABLE_FMT = 
                    "<table class='table'>"
                +       "<thead>{0}</thead>"
                +       "<tbody>"
                +           "<tr class='fibbo-elem'>{1}</tr>"
                +           "<tr class='facto-elem'>{2}</tr>"
                +           "<tr class='area-elem'>{3}</tr>"
                +           "<tr class='vol-elem'>{4}</tr>"
                +       "</tbody>"
                +       "<tfoot>"
                +           "<tr class='sum-elems'>{5}</tr>"
                +       "</tfoot>"
                +   "</table>";

            var result = "";

            result = TABLE_FMT.format(
                index_record
              , fibbo_record
              , fact_record
              , area_record
              , vol_record
              , sum_record);

            return result;
        }

        function RenderOn(elementID, tableHTML, fibbos_sum, factos_sum, areas_sum, vols_sum)
        {
            YUI().use('none', 'event', function(Y){
                Y.one(elementID).setHTML(tableHTML);
                Y.one("#row1-sum").setHTML(fibbos_sum);
                Y.one("#row2-sum").setHTML(factos_sum);
                Y.one("#row3-sum").setHTML(areas_sum);
                Y.one("#row4-sum").setHTML(vols_sum);
            });
        }

        function ComputeDataSet(maxlength){
                var AREA_ROW_NUMBER = 3;

                var tableHTML = "";

                //Concats
                var fibbo_record = "";
                var index_record = "";
                var fact_record  = "";
                var area_record  = "";
                var vol_record   = "";
                var sum_record   = "";

                //Accums
                var fibbos_sum = 0;
                var factos_sum = 0;
                var areas_sum  = 0;
                var vols_sum   = 0;

                //Operation retrieve
                var fibbos = ComputeFibboSeries(maxlength);
                
                for (var i = 0; i < maxlength; i++) 
                {
                    //aux variables
                    var col_output = i + 1;
                    var fibbo_val  = fibbos[i];
                    var fact_val   = GetFactorial(col_output);
                    var area_val   = col_output * AREA_ROW_NUMBER * 10;
                    var vol_val    = col_output * fact_val;
                    var sum_val    =  col_output + fibbo_val + fact_val + area_val + vol_val;

                    
                    //string building
                    var index_field = "<td class='center'>Col <em>" + col_output + "</em></td>";

                    var fibbo_field = "<td class='center'>" + fibbo_val + "</td>";
                    var fact_field  = "<td class='center'>" + fact_val + "</td>";
                    var area_field  = "<td class='center'>" + area_val + "</td>";
                    var vol_field   = "<td class='center'>" + vol_val + "</td>";
                    var sum_field   = "<td class='center'><em>SUM " + sum_val + "</em></td>";

                        //CONCAT
                    index_record += index_field;
                    fibbo_record += fibbo_field;
                    fact_record  += fact_field;
                    area_record  += area_field;
                    vol_record   += vol_field;
                    sum_record   += sum_field;
                        //SUMATORIES
                    factos_sum += fact_val;
                    fibbos_sum += fibbo_val;
                    areas_sum  += area_val;
                    vols_sum   += vol_val;
                }

                tableHTML = GetTableHTML(
                    index_record
                  , fibbo_record
                  , fact_record
                  , area_record
                  , vol_record
                  , sum_record);

                

                RenderOn(
                    "#spreadsheet"
                  , tableHTML
                  , fibbos_sum
                  , factos_sum
                  , areas_sum
                  , vols_sum);
        }

        function ComputeFibboSeries(maxlength){
            var var1 = 0;
            var var2 = 1;
            var var3;

            var result = []

            if (maxlength >= 1)
                result.push(var1);

            if (maxlength >= 2)
                result.push(var2);
 
            for(var i=3; i <= maxlength;i++) 
            {
                var3 = var1 + var2;
                var1 = var2;
                var2 = var3;

                result.push(var3);
            }

            return result;
        }

        function GetFactorial(number) {
            var result = 1;

            if (number == 0 || number == 1)
                result = 1
            else 
                for (var i = 2; i <= number; i++)
                    result *= i;

            return result;
        }